{
    "name" : "Raloy Reports",
    "version" : "1.0",
    "author" : "Raloy Lubricantes, S.A. de C.V.",
    "description" : """
        Módulo para personalizar los reportes de Raloy
    """,
    "website" : "http://www.raloylubricantes.mx",
    "license" : "AGPL-3",
    "depends" : [
        "l10n_mx_einvoice_report"
    ],
    "data" : [
        "report/invoice_raloy.xml",
    ],
    "installable" : True,
}